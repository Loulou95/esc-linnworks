<?php
namespace Esc\Linnworks\Providers;

use Illuminate\Support\ServiceProvider;
use Esc\Linnworks\API;

class LinnworksServiceProvider extends ServiceProvider {

   public function register()
   {
        $this->publishes([
            __DIR__.'/../config/esc_linnworks.php' => config_path('esc_linnworks.php'),
        ]);

        $this->app->singleton(API::class, function($app) {
            $apiKey = \Config::get('esc_linnworks.credentials.api_key');
            $apiSecret = \Config::get('esc_linnworks.credentials.api_secret');
            return new API($apiKey, $apiSecret);
        });
   }

   public function boot() {

   }
}
