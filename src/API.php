<?php
namespace Esc\Linnworks;


/*

    Usage:

    // Install
    $api = new API($apiKey, $apiSecret);
    $installUrl = $api->createInstallUrl();

    redirect($installUrl);

    // Post-install
    $token = user input;
    $api = new API($apiKey, $apiSecret);
    $accessToken = $api->authorize($token);

    $userData = $api->userData; // locality, expiration date, access token, user name, etc.


    // Use API
    $api->call('get', 'Dashboard/TopProducts');
    $api->get('Dashboard/TopProducts');

*/

class API {
    const API_ROOT = 'https://api.linnworks.net/api';

    protected $apiKey;
    protected $apiSecret;
    protected $accessToken;
    protected $userData;

    public function __construct($apiKey = null, $apiSecret = null) {
        $this->config($apiKey, $apiSecret);
    }

    public function config($apiKey, $apiSecret) {
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
    }


    public function createInstallUrl() {
        if (!$this->apiKey) {
            throw new \Exception('Cannot create install url without API key.');
        }
        return 'http://apps.linnworks.net/Authorization/Install/'.$this->apiKey;
    }

    /**
     * Authorizes the API.
     * @param token The user's install token
     */
    public function authorize($token) {
        $url = '/Auth/AuthorizeByApplication';

        if (!$this->apiKey) {
            throw new \Exception('Cannot authorize without API key.');
        }
        if (!$this->apiSecret) {
            throw new \Exception('Cannot authorize without API secret.');
        }
        if (!$token) {
            throw new \Exception('Cannot authorize without an install token.');
        }

        $res = $this->post($url, [
            'applicationId' => $this->apiKey,
            'applicationSecret' => $this->apiSecret,
            'token' => $token
        ], [
            'all_data' => true,
            'fail_on_error' => false
        ]);

        $this->userData = $res;
        $this->accessToken = $res->Token;

        return $this->accessToken;
    }




    public function get($url, $data = [], $options = []) {
        return $this->call('get', $url, $data, $options);
    }

    public function post($url, $data = [], $options = []) {
        return $this->call('post', $url, $data, $options);
    }

    public function call($method, $url, $data = [], $options = []) {
        // Setup options
	    $defaults = [
		    'charset'       => 'UTF-8',
			'headers'       => array(),
	        'fail_on_error'   => TRUE,
	        'return_array'   => FALSE,
	        'all_data'       => FALSE,
            'verify_data'    => TRUE,
	    ];
        $options = array_merge($defaults, $options);


        // Setup headers
        $defaultHeaders = [];
	    $defaultHeaders[] = 'Content-Type: application/x-www-form-urlencoded; charset=' . $options['charset'];
	    $defaultHeaders[] = 'Accept: application/json';

        if ($this->accessToken) {
		    $defaultHeaders[] = 'Authorization: ' . $this->accessToken;
        }
        $headers = array_merge($defaultHeaders, $options['headers']);

        // Setup URL
        if ($options['verify_data']) {
            $url = 'https://api.linnworks.net/api/'.$url;
        }

        // Setup CURL
        $ch = curl_init();
        $curlOpts = array(
            CURLOPT_RETURNTRANSFER  => TRUE,
            CURLOPT_URL             => $url,
            CURLOPT_HTTPHEADER      => $headers,
            CURLOPT_CUSTOMREQUEST   => strtoupper($method),
            CURLOPT_ENCODING        => '',
            CURLOPT_USERAGENT       => 'Eastside Co Linnworks API Wrapper',
            CURLOPT_FAILONERROR     => $options['fail_on_error'],
            CURLOPT_VERBOSE         => $options['all_data'],
            CURLOPT_HEADER          => 1
        );

        if ($data) {
            $curlOpts[CURLOPT_POSTFIELDS] = http_build_query($data);
        }

        curl_setopt_array($ch, $curlOpts);

        // Make request
        $response = curl_exec($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $result = json_decode(substr($response, $headerSize), $options['return_array']);

        $info = array_filter(array_map('trim', explode("\n", substr($response, 0, $headerSize))));

        // Parse errors
        $returnError = [
            'number' => curl_errno($ch),
            'msg' =>  curl_error($ch)
        ];
        $test1231231231 = curl_getinfo($ch);
        curl_close($ch);


	    if ($returnError['number'])
	    {
		    throw new \Exception('ERROR #' . $returnError['number'] . ': ' . $returnError['msg']);
	    }


        // Parse extra info
        $returnInfo = null;
        if ($options['all_data']) {
            foreach($info as $k => $header)
            {
    	        if (strpos($header, 'HTTP/') > -1)
    	        {
                    $returnInfo['HTTP_CODE'] = $header;
                    continue;
                }
                list($key, $val) = explode(':', $header);
                $returnInfo[trim($key)] = trim($val);
            }
        }

        if ($options['all_data']) {
            if ($options['return_array']) {
                $result['_ERROR'] = $returnError;
                $result['_INFO'] = $returnInfo;
            } else {
                $result->_ERROR = $returnError;
                $result->_INFO = $returnInfo;
            }
            return $result;
        }
        return $result;
    }


}
